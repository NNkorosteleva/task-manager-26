package ru.tsc.korosteleva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.api.repository.IUserOwnedRepository;
import ru.tsc.korosteleva.tm.api.service.IUserOwnedService;
import ru.tsc.korosteleva.tm.enumerated.Sort;
import ru.tsc.korosteleva.tm.exception.entity.ModelNotFoundException;
import ru.tsc.korosteleva.tm.exception.field.IdEmptyException;
import ru.tsc.korosteleva.tm.exception.field.IndexIncorrectException;
import ru.tsc.korosteleva.tm.exception.user.UserIdEmptyException;
import ru.tsc.korosteleva.tm.model.AbstractUserOwnedModel;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModel, R extends IUserOwnedRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    public AbstractUserOwnedService(@NotNull final R repository) {
        super(repository);
    }

    @NotNull
    @Override
    public M add(@Nullable final String userId, @Nullable final M model) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.add(userId, model);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        return repository.findAll(userId);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Comparator<M> comparator) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        if (comparator == null) return findAll(userId);
        return repository.findAll(userId, comparator);
    }

    @NotNull
    @Override
    public List<M> findAll(@Nullable final String userId, @Nullable final Sort sort) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        if (sort == null) return findAll(userId);
        return repository.findAll(userId, sort);
    }

    @Nullable
    @Override
    public M findOneById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable Optional<M> model = Optional.ofNullable(repository.findOneById(userId, id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Nullable
    @Override
    public M findOneByIndex(@Nullable final String userId, @Nullable final Integer index) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize(userId))
                .orElseThrow(IndexIncorrectException::new);
        @Nullable Optional<M> model = Optional.ofNullable(repository.findOneByIndex(userId, index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Nullable
    @Override
    public M remove(@Nullable final String userId, @Nullable final M model) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(model).orElseThrow(ModelNotFoundException::new);
        return repository.remove(userId, model);
    }

    @Nullable
    @Override
    public M removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable Optional<M> model = Optional.ofNullable(repository.removeById(userId, id));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Nullable
    @Override
    public M removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(index).orElseThrow(IndexIncorrectException::new);
        Optional.of(index)
                .filter(i -> i > 0 && i < repository.getSize(userId))
                .orElseThrow(IndexIncorrectException::new);
        @Nullable Optional<M> model = Optional.ofNullable(repository.removeByIndex(userId, index));
        return model.orElseThrow(ModelNotFoundException::new);
    }

    @Override
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        repository.clear(userId);
    }

    @Override
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        return repository.getSize(userId);
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        if (id == null || id.isEmpty()) return false;
        return repository.existsById(userId, id);
    }

}
