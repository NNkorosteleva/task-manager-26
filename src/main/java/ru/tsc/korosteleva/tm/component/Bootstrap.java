package ru.tsc.korosteleva.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.tsc.korosteleva.tm.api.repository.*;
import ru.tsc.korosteleva.tm.api.service.*;
import ru.tsc.korosteleva.tm.command.AbstractCommand;
import ru.tsc.korosteleva.tm.enumerated.Status;
import ru.tsc.korosteleva.tm.exception.system.ArgumentNotSupportedException;
import ru.tsc.korosteleva.tm.exception.system.CommandNotSupportedException;
import ru.tsc.korosteleva.tm.repository.*;
import ru.tsc.korosteleva.tm.service.*;
import ru.tsc.korosteleva.tm.util.DateUtil;
import ru.tsc.korosteleva.tm.util.SystemUtil;
import ru.tsc.korosteleva.tm.util.TerminalUtil;
import ru.tsc.korosteleva.tm.model.User;

import java.io.File;
import java.lang.reflect.Modifier;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Set;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String PACKAGE_COMMANDS = "ru.tsc.korosteleva.tm.command";

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @Getter
    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskRepository projectTaskRepository = new ProjectTaskRepository(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectTaskRepository);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository(projectRepository, taskRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthRepository authRepository = new AuthRepository();

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(userService, authRepository, propertyService);

    {
        @NotNull final Reflections reflections = new Reflections(PACKAGE_COMMANDS);
        @NotNull final Set<Class<? extends AbstractCommand>> classes =
                reflections.getSubTypesOf(AbstractCommand.class);
        for (@NotNull final Class<? extends AbstractCommand> clazz : classes) registry(clazz);

    }

    public void run(@Nullable final String[] args) {
        if (processArgument(args)) close();
        initPID();
        initData();
        showProcess();
    }

    public void showProcess() {
        showWelcome();
        while (true) {
            try {
                System.out.println("ENTER COMMAND:");
                @NotNull final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    public void processCommand(@NotNull final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException(command);
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
    }

    public boolean processArgument(@Nullable final String[] args) {
        if (args == null || args.length == 0) return false;
        @Nullable final String arg = args[0];
        try {
            processArgument(arg);
        } catch (Exception e) {
            System.err.println(e.getMessage());
            System.out.println("[FAIL]");
            close();
        }
        return true;
    }

    public void processArgument(@Nullable final String arg) {
        if (arg == null || arg.isEmpty()) throw new ArgumentNotSupportedException();
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException(arg);
        abstractCommand.execute();
    }

    public void showWelcome() {
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void close() {
        System.exit(0);
    }

    @SneakyThrows
    private void registry(@NotNull final Class<? extends AbstractCommand> clazz) {
        if (Modifier.isAbstract(clazz.getModifiers())) return;
        if (!AbstractCommand.class.isAssignableFrom(clazz)) return;
        @NotNull final AbstractCommand command = clazz.newInstance();
        registry(command);
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }


    private void initData() {
        @NotNull User usualUser = userService.create("test", "test", "test@test.te");
        @NotNull User adminUser = userService.create("admin", "admin", "admin@admin.ad", "ADMIN");
        taskService.create(usualUser.getId(), "DEMO TASK", "DEMO DESCRIPTION", DateUtil.toDate("10.01.2019"), DateUtil.toDate("10.10.2019")).setStatus(Status.NOT_STARTED);
        taskService.create(adminUser.getId(), "TEST TASK", "TEST DESCRIPTION", DateUtil.toDate("09.12.2020"), DateUtil.toDate("10.12.2020")).setStatus(Status.IN_PROGRESS);
        taskService.create(adminUser.getId(), "CHECK TASK", "CHECK DESCRIPTION", DateUtil.toDate("10.01.2019"), DateUtil.toDate("20.01.2019")).setStatus(Status.COMPLETED);
        taskService.create(usualUser.getId(), "CHECKED TASK", "CHECKED DESCRIPTION", DateUtil.toDate("01.04.2019"), DateUtil.toDate("05.04.2019")).setStatus(Status.IN_PROGRESS);
        projectService.create(usualUser.getId(), "DEMO PROJECT", "DEMO DESCRIPTION", DateUtil.toDate("12.12.2019"), DateUtil.toDate("12.12.2020")).setStatus(Status.NOT_STARTED);
        projectService.create(usualUser.getId(), "TEST PROJECT", "TEST DESCRIPTION", DateUtil.toDate("11.11.2020"), DateUtil.toDate("15.11.2020")).setStatus(Status.COMPLETED);
        projectService.create(usualUser.getId(), "CHECK PROJECT", "CHECK DESCRIPTION", DateUtil.toDate("10.08.2019"), DateUtil.toDate("10.10.2019")).setStatus(Status.IN_PROGRESS);
        projectService.create(adminUser.getId(), "CHECKED PROJECT", "CHECKED DESCRIPTION", DateUtil.toDate("05.07.2021"), DateUtil.toDate("10.07.2021")).setStatus(Status.COMPLETED);
    }

}