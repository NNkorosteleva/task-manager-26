package ru.tsc.korosteleva.tm.exception.user;

public final class AccessDeniedException extends AbstractUserException {

    public AccessDeniedException() {
        super("Error! Access is denied.");
    }

}
