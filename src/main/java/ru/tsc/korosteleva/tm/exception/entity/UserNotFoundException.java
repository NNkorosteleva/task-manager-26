package ru.tsc.korosteleva.tm.exception.entity;

import ru.tsc.korosteleva.tm.exception.AbstractException;

public class UserNotFoundException extends AbstractException {

    public UserNotFoundException() {
        super("Error! User not found.");
    }

}
