package ru.tsc.korosteleva.tm.exception.user;

public final class EmailEmptyException extends AbstractUserException {

    public EmailEmptyException() {
        super("Error! Email is empty.");
    }

}
