package ru.tsc.korosteleva.tm.exception.field;

public final class DateIncorrectException extends AbstractFieldException {

    public DateIncorrectException() {
        super("Error! Date is incorrect.");
    }

    public DateIncorrectException(String value) {
        super("Error! Date ``" + value + "`` is incorrect.");
    }

}
