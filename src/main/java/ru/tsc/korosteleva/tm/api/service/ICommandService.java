package ru.tsc.korosteleva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.command.AbstractCommand;

import java.util.Collection;

public interface ICommandService {

    void add(@Nullable AbstractCommand command);

    @NotNull
    Collection<AbstractCommand> getTerminalCommands();

    @Nullable
    AbstractCommand getCommandByName(@Nullable String name);

    @Nullable
    AbstractCommand getCommandByArgument(@Nullable String argument);
}
