package ru.tsc.korosteleva.tm.api.service;

import ru.tsc.korosteleva.tm.api.repository.IRepository;
import ru.tsc.korosteleva.tm.model.AbstractModel;

public interface IService<M extends AbstractModel> extends IRepository<M> {

}
