package ru.tsc.korosteleva.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.model.User;

public interface IUserRepository extends IRepository<User> {

    @Nullable
    User updateUser(@NotNull String id,
                    @NotNull String firstName,
                    @NotNull String lastName,
                    @NotNull String middleName);

    @Nullable
    User findOneByLogin(@NotNull String login);

    @Nullable
    User findOneByEmail(@NotNull String email);

    @Nullable
    User remove(@NotNull User user);

    @Nullable
    User removeById(@NotNull String id);

    @Nullable
    User removeByLogin(@NotNull String login);

    void lockUserByLogin(@NotNull String login);

    void unlockUserByLogin(@NotNull String login);

}
