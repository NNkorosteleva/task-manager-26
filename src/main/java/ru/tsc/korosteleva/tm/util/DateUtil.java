package ru.tsc.korosteleva.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.korosteleva.tm.exception.field.DateIncorrectException;

import java.text.SimpleDateFormat;
import java.util.Date;

public interface DateUtil {

    @NotNull
    String PATTERN = "dd.MM.yyyy";

    @NotNull
    SimpleDateFormat FORMATTER = new SimpleDateFormat(PATTERN);

    @Nullable
    static Date toDate(final String value) {
        try {
            return FORMATTER.parse(value);
        } catch (Exception e) {
            throw new DateIncorrectException(value);
        }
    }

    @NotNull
    static String toString(final Date value) {
        if (value == null) return "";
        return FORMATTER.format(value);
    }

}
